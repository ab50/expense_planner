import 'package:flutter/material.dart';

import 'package:flutter_assignment/widgets/user_transactions.dart';
class NewTransaction extends StatelessWidget {
  final addT;
  final titleController = TextEditingController();
  final amountController = TextEditingController();

  NewTransaction(this.addT);
  void submitData() {
    final enteredTitle = titleController.text;
    final enteredAmount = double.parse(amountController.text);
    if (enteredTitle.isEmpty || enteredAmount <= 0) {
      return;
    }
    addT(
      enteredTitle,
      enteredAmount,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            TextField(
              decoration: InputDecoration(labelText: 'Purchase title'),
              controller: titleController,
            ),
            TextField(
              decoration: InputDecoration(labelText: 'Purchase amount'),
              controller: amountController,
              keyboardType: TextInputType.number,
              onSubmitted: (_) => submitData(),
            ),
            FlatButton(
              child: Text('Add Transaction'),
              textColor: Colors.purple,
              onPressed: submitData,
            ),
          ],
        ),
      ),
    );
  }
}
