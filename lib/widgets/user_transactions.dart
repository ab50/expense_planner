import 'package:flutter/material.dart';

import 'package:flutter_assignment/widgets/new_transaction.dart';
import 'package:flutter_assignment/widgets/transaction_list.dart';
import 'package:flutter_assignment/models/transaction.dart';

class UserTransactions extends StatefulWidget {
  @override
  _UserTransactionsState createState() => _UserTransactionsState();
}
class _UserTransactionsState extends State<UserTransactions> {
  final List<Transaction> _userTransactions = [
    Transaction(
      id: 't1',
      title: 'New Shoes',
      amount: 69.99,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't2',
      title: 'Weekly Groceries',
      amount: 16.53,
      date: DateTime.now(),
    )
  ];

  void _addNewTransaction(String Ttitle, double Tamount) {
    final newT = Transaction(
        title: Ttitle,
        amount: Tamount,
        date: DateTime.now(),
        id: DateTime.now().toString(),
    );
    setState(() {
      _userTransactions.add(newT);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        NewTransaction(_addNewTransaction),
        TransactionList(_userTransactions),
      ],
    );
  }
}
