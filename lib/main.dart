import 'package:flutter/material.dart';
import 'package:flutter_assignment/widgets/user_transactions.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter App'),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.add),
                onPressed: () {}
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                width: double.infinity,
                child: Card(
                  child: Text('CHART!'),
                  elevation: 5,
                  color: Colors.yellow,
                ),
              ), // This will hold the chart
              UserTransactions()
            ],
          ),
        ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              backgroundColor: Colors.purple,
              onPressed: () {},
          ),
      ),
    );
  }
}